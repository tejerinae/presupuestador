import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Presupuesto } from '../models/presupuesto';
import { Cliente } from '../models/cliente';
import { Producto } from '../models/producto';
import { Conjunto } from '../models/conjunto';
import { Aplicacion } from '../models/aplicacion';
import { Obra } from '../models/obra';
import { Observable } from 'rxjs';

@Injectable()
export class PresupuestoService {
  resp: any;
  constructor(private http: HttpClient) {}
  // dirBase = 'http://localhost/symphart/public';
  dirBase = 'http://ventilar.25u.com:10080/symphart/public';
  listar() {
    this.resp = this.http.get<Presupuesto[]>(this.dirBase + '/presupuestos');
    return this.resp;
  }
  traerPresupuesto(id: number) {
    this.resp = this.http.get<Presupuesto>(this.dirBase + '/presupuesto/' + id);
    return this.resp;
  }
  borrarPresupuesto(presupuesto: Presupuesto) {
    this.resp = this.http.delete(this.dirBase + '/presupuesto/' + presupuesto.cliente + '/'
      + presupuesto.obra + '/' + presupuesto.version);
    return this.resp;
  }
  listarVersiones(idCliente: number, idObra: number ) {
    this.resp = this.http.get<string[]>(this.dirBase + '/presupuesto/' + idCliente + '/' + idObra);
    return this.resp;
  }
  listarProducto() {
    this.resp = this.http.get<Producto[]>(this.dirBase + '/productos');
    return this.resp;
  }
  listarConjunto() {
    this.resp = this.http.get<Conjunto[]>(this.dirBase + '/conjuntospadre');
    return this.resp;
  }
  listarAplicaciones() {
    this.resp = this.http.get<Aplicacion[]>(this.dirBase + '/aplicaciones');
    return this.resp;
  }
  listarClientes() {
    this.resp = this.http.get<Cliente[]>(this.dirBase + '/clientes');
    return this.resp;
  }
  traerCliente(nombre: String) {
    this.resp = this.http.get<Cliente[]>(this.dirBase + '/cliente/' + nombre);
    return this.resp;
  }
  listarObras(id) {
    this.resp = this.http.get<Obra[]>(this.dirBase + '/obra/' + id);
    return this.resp;
  }
  traerProducto(id: number) {
    this.resp = this.http.get<Producto[]>(this.dirBase + '/producto/' + id);
    return this.resp;
  }
  guardar(item: Presupuesto) {
    const cache = [];
    const json = JSON.stringify(item, function(key, value) {
      if (typeof value === 'object' && value !== null) {
          if (cache.indexOf(value) !== -1) {
              // Duplicate reference found
              try {
                  // If this value does not reference a parent it can be deduped
                  return JSON.parse(JSON.stringify(value));
              } catch (error) {
                  // discard key if value cannot be deduped
                  return;
              }
          }
          // Store value in our collection
          cache.push(value);
      }
      return value;
    });
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    this.resp = this.http.post(this.dirBase + '/presupuesto', json, {headers: headers});
    return this.resp;
  }
}
