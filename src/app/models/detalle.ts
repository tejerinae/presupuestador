export class Detalle {
    id: number;
    idpresupuesto: number;
    codigoparticular: string;
    idPadre: any;
    id_conjunto: number;
    descripcion: string;
    tipoPrecio: string;
    cantidad: number;
    precio: number;
    codigoaplicacion: number;
    m1: number;
    m2: number;
    m3: number;
    m4: number;
    m5: number;
    m1Desc: string;
    m2_desc: string;
    m3_desc: string;
    m4_desc: string;
    m5_desc: string;
    activo: number;
    articulo: any;
}

