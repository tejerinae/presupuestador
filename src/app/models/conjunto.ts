export class Conjunto {
    idConjunto: number;
    idPadre: number;
    descripcion: string;
    codigoparticular: string;
    cantidad: number;
}
