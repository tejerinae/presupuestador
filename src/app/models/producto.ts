export class Producto {
    codigoparticular: string;
    descripcion: string;
    blockd: number;
    unidad: string;
    tipoPrecio: string;
    rubro: string;
    subrubro: string;
    precios: any
}

