import { Detalle } from "./detalle";

export class Presupuesto {
    id:number ;
    cliente: string;
    obra: string;
    version: string;
    m1: number = 0;
    m2: number = 0;
    m3: number = 0;
    m4: number = 0;
    m5: number = 0;
    m1Desc: string;
    m2_desc: string;
    m3_desc: string;
    m4_desc: string;
    m5_desc: string;
    items: any[];
    nombre: any;
}
