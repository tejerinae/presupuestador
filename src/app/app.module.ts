import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { DataTableModule, SharedModule, ButtonModule, MessageModule, MessagesModule, MessageService } from 'primeng/primeng';
import { TableModule } from 'primeng/table';
import { TreeTableModule } from 'primeng/treetable';
import { RouterModule, Routes } from '@angular/router';
import { ToastModule } from 'primeng/toast';

import { AppComponent } from './app.component';
import { DetallePresupuestoComponent } from './components/detalle-presupuesto/detalle-presupuesto.component';
import { PresupuestoService } from './services/presupuesto.service';
import { HttpClientModule } from '@angular/common/http';
import { DialogModule } from 'primeng/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ConfirmationService} from 'primeng/api';
import {CheckboxModule} from 'primeng/checkbox';
import {AutoCompleteModule} from 'primeng/autocomplete';
import {InputTextModule} from 'primeng/inputtext';
import {InputMaskModule} from 'primeng/inputmask';
import {ProgressSpinnerModule} from 'primeng/progressspinner';
import {TriStateCheckboxModule} from 'primeng/tristatecheckbox';

/*const appRoutes: Routes = [
  { path: 'detalle/:id', component: DetallePresupuestoComponent },
  { path: '', redirectTo: '/detalle/0', pathMatch: 'full' },
  { path: '**', redirectTo: '/detalle/0', pathMatch: 'full' }
]*/

const appRoutes: Routes = [
  { path: '', component: DetallePresupuestoComponent },
  { path: '**', redirectTo: ''}
];

@NgModule({
  declarations: [
    AppComponent,
    DetallePresupuestoComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    DataTableModule,
    SharedModule,
    ButtonModule,
    TableModule,
    TreeTableModule,
    DialogModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    ConfirmDialogModule,
    CheckboxModule,
    AutoCompleteModule,
    InputTextModule,
    InputMaskModule,
    ProgressSpinnerModule,
    TriStateCheckboxModule,
    MessagesModule,
    MessageModule,
    ToastModule
  ],
  exports: [
    RouterModule,
    DialogModule,
    ConfirmDialogModule,
    CheckboxModule
  ],
  providers: [PresupuestoService, ConfirmationService, MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
