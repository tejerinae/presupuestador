import { Component, OnInit } from '@angular/core';
import { PresupuestoService } from '../../services/presupuesto.service';
import { Presupuesto } from '../../models/presupuesto';
import { Cliente } from '../../models/cliente';
import { Detalle } from '../../models/detalle';
import { ActivatedRoute } from '@angular/router';
import { Producto } from '../../models/producto';
import { Conjunto } from '../../models/conjunto';
import { Obra } from '../../models/obra';
import { TreeNode, SortEvent, Message, MessageService } from 'primeng/components/common/api';
import { Aplicacion } from '../../models/aplicacion';
import { ConfirmationService } from 'primeng/api';

@Component({
    selector: 'app-detalle-presupuesto',
    templateUrl: './detalle-presupuesto.component.html',
    styleUrls: ['./detalle-presupuesto.component.css']
})

export class DetallePresupuestoComponent implements OnInit {
    presupuesto:  Presupuesto;
    productos:  Producto[] ;
    conjuntos:  Conjunto[] ;
    aplicaciones:  Aplicacion[] ;
    clientes: Cliente[];
    obras: Obra[];
    detalle:  TreeNode[] ;
    nuevoDetalle:  TreeNode[] ;
    selectednode1:  TreeNode[] ;
    nodeDescripcion:  String ;
    nodeCantidad:  Number ;
    nodePrecio:  Number ;
    rowGroupMetadata: any;
    total: any = 0;
    display0 = false;
    display1 = false;
    display2 = false;
    confirma = false;
    dModifica = false;
    showObra = true;
    showVersion = true;
    displayWait = false;
    cols: any;
    presupuestoSeleccionado: Presupuesto;
    productoSeleccionado: Producto;
    conjuntoSeleccionado: Conjunto;
    aplicacionSeleccionado: Aplicacion;
    cantidad: Number;
    cliente: string;
    obra: string;
    filteredClientes: any[];
    codigocliente: any;
    filteredObras: any[];
    total1: any = 0;
    total2: any = 0;
    total3: any = 0;
    total4: any = 0;
    total5: any = 0;
    listaPresupuestos: Presupuesto[] ;
    codCliente: any;
    codObra: any;
    version: string;
    listaOrdenada: Detalle[] = [];
    lista_ord = false;
    checkedVal: boolean = null;
    versiones: any;
    filteredVersiones: any[];
    msgs: Message[] = [];

    constructor(private presupuestoService: PresupuestoService, private route: ActivatedRoute
        , private confirmationService: ConfirmationService, private messageService: MessageService) {
    }

    ngOnInit() {
        this.detalle = [];
        this.route.params.subscribe( params => this.getPresupuesto(params.id) );
        this.presupuestoService.listarClientes().subscribe(
            data => {
                this.clientes = data;
            }
        );
    }

    filterClientes(event) {
        this.filteredClientes = [];
        for (let i = 0; i < this.clientes.length; i++) {
            const cliente = this.clientes[i];
            if (cliente.nombre.toLowerCase().indexOf(event.query.toLowerCase()) === 0) {
                this.filteredClientes.push(cliente);
            }
        }
    }

    selectedCliente(event) {
        this.cliente = '';
        this.codCliente = event.codigocliente;
        this.presupuestoService.listarObras(event.codigocliente).subscribe(
            data => {
                this.obras = data;
                this.cliente = event.nombre;
                this.obra = '';
                if (this.obras.length > 0) {
                    this.showObra = false;
                }
            }
        );
    }

    filterObras(event) {
        this.filteredObras = [];
        for (let i = 0; i < this.obras.length; i++) {
            const obrat = this.obras[i];
            if (obrat.nombre.toLowerCase().indexOf(event.query.toLowerCase()) === 0) {
                this.filteredObras.push(obrat);
            }
        }
    }

    selectedObra(event) {
        this.obra = '';
        this.codObra = event.codigoproyecto;
        this.showVersion = false;
        this.presupuestoService.listarVersiones(this.codCliente, this.codObra).subscribe(
            data => {
                this.versiones = data;
                this.obra = event.nombre;
                /*if (this.obras.length>0){
                    this.showObra=false;
                }*/

            }
        );
    }

    filterVersiones(event) {
        this.filteredVersiones = [];
        for (let i = 0; i < this.versiones.length; i++) {
            const version = this.versiones[i];
            if (version.version.toLowerCase().indexOf(event.query.toLowerCase()) === 0) {
                this.filteredVersiones.push(version);
            }
        }
    }

    selectedVersion(event) {
        this.version = '';
        this.version = event.version;
    }

    getPresupuesto(id: number) {
        if (id > 0) {
            this.presupuestoService.traerPresupuesto(id).subscribe(
                data => {
                    this.presupuesto = data;
                    this.nuevoDetalle = this.presupuesto.items;
                    this.nuevoDetalle.forEach(element => {
                        this.detalle.push(element);
                    });
                    this.detalle = [...this.detalle];
                    this.calcularTotalesIni();
                }
            );
        } else {
            this.presupuesto = new Presupuesto();
        }
        this.display0 = false;
    }

    showProductoDialog() {
        this.presupuestoService.listarProducto().subscribe(
            data => {
                this.productos = data;
                this.displayWait = false;
                this.display1 = true;
            }
        );
        this.presupuestoService.listarAplicaciones().subscribe(
            data => {
                this.aplicaciones = data;
            }
        );
        this.cols = [
            { field: 'codigoparticular', header: 'Codigo' },
            { field: 'descripcion', header: 'Descripcion' },
            { field: 'rubro', header: 'Rubro' }
        ];
        this.displayWait = true;
        this.cantidad = 1;
    }

    showConjuntoDialog() {
        this.presupuestoService.listarConjunto().subscribe(
            data => {
                this.conjuntos = data;
                this.displayWait = false;
                this.display2 = true;
            }
        );
        this.presupuestoService.listarAplicaciones().subscribe(
            data => {
                this.aplicaciones = data;
            }
        );
        this.cols = [
            { field: 'idConjunto', header: 'Conjunto' },
            { field: 'descripcion', header: 'Descripcion' },
            { field: 'rubro', header: 'Rubro' }
        ];
        this.displayWait = true;
        this.cantidad = 1;
    }

    showPresupuestoDialog() {
        this.presupuestoService.listar().subscribe(
            data => {
                for (let _i = 0; _i < data.length; _i++) {
                    // console.log(data);
                    data[_i].cliente = data[_i].cliente.nombre;
                    data[_i].obra = data[_i].obra.nombre;
                }
                this.listaPresupuestos = data;
                this.displayWait = false;
                this.display0 = true;
            }
        );
        this.cols = [
            { field: 'id', header: 'Id', style: '10%'},
            { field: 'cliente', header: 'Cliente', style: '30%'},
            { field: 'obra', header: 'Obra', style: '30%'},
            { field: 'version', header: 'Versión', style: '10%'}
        ];
        this.displayWait = true;
    }
    seleccionarProducto() {
        const nuevoItem = this.productoToTreeNode(this.productoSeleccionado);
        // console.log(nuevoItem);
        nuevoItem.data.aplicacion =  this.aplicacionSeleccionado.descripcion;
        nuevoItem.data.codigoAplicacion =  this.aplicacionSeleccionado.codigo;
        if (this.selectednode1 == null || this.selectednode1['parent'] == null) {
            this.detalle.push(nuevoItem);
        } else {
            this.selectednode1['parent'].children.push(nuevoItem);
        }
        this.calcularTotalesIni();
        this.display1 = false;
    }

    completarConjunto(node: TreeNode) {
        if (node.children.length > 0) {
            node.children.forEach(element => {
                this.completarConjunto(element);
                element.data.aplicacion =  this.aplicacionSeleccionado.descripcion;
                element.data.codigoAplicacion =  this.aplicacionSeleccionado.codigo;
                element.data.total = element.data.precio * element.data.cantidad;
            });
            node.data.aplicacion =  this.aplicacionSeleccionado.descripcion;
            node.data.codigoAplicacion =  this.aplicacionSeleccionado.codigo;
            node.data.total = node.data.precio * node.data.cantidad;
        }
    }

    seleccionarConjunto() {
        const nuevoItem = <TreeNode>this.conjuntoSeleccionado;
        this.completarConjunto(nuevoItem);
        // this.calcularTotales(nuevoItem);
        if (this.selectednode1 == null || this.selectednode1['parent'] == null) {
            this.detalle.push(nuevoItem);
        } else {
            this.selectednode1['parent'].children.push(this.conjuntoSeleccionado);
        }
        this.calcularTotalesIni();
        this.display2 = false;
    }

    nodeSelect(event) {
        // console.log(event);
    }

    checked(currentNode: Array<TreeNode>, node: any, name, var1: boolean= null) {
        currentNode.forEach((element) => {
            if (var1 != null) {
                element.data[name] = var1;
            } else {
                if (node.node.data === element.data) {
                    element.data[name] = !element.data[name];
                    if (element.children.length > 0) {
                        this.checked(element.children, node, name, element.data[name]);
                    }
                }
            }
            if (currentNode.length > 0) {
                this.checked(element.children, node, name, var1);
            }
        });
        this.calcularTotalesIni();
        // this.selectednode1 = null;
    }

    deleteNode(currentNode: Array<TreeNode>, node: any) {
        currentNode.forEach((element, index) => {
            if (currentNode.length > 0) {
                this.deleteNode(element.children, node);
                if (node.node.data === element.data) {
                    if (node.node.parent == null) {
                        console.log(this.detalle);
                        this.detalle.splice(index, 1);
                    } else {
                        node.node.parent.children.splice(index, 1);
                    }
                    this.calcularTotalesIni();
                }
            }
        });
        this.selectednode1 = null;
        // console.log(this.detalle);
    }

    confirm2(currentNode: Array<TreeNode>, node: any) {
        this.confirmationService.confirm({
            message: 'Realmente desea eliminar este articulo/conjunto?',
            header: 'Confirmación de borrado',
            icon: 'pi pi-info-circle',
            accept: () => {
                this.deleteNode(currentNode, node);
                console.log('borra');
            },
            reject: () => {
                console.log('No borra');
            }
        });
    }

    calcularTotalesIni() {
        this.total = 0;
        this.total1 = 0;
        this.total2 = 0;
        this.total3 = 0;
        this.total4 = 0;
        this.total5 = 0;
        this.detalle.forEach((element) => {
            if (element.children.length > 0) {
                this.calcularTotales(element);
            } else {
                let totalLinea = 0;
                totalLinea = Number(element.data.precio) * Number(element.data.cantidad);
                element.data.precio = totalLinea;
                element.data.total = (element.data.precio * element.data.cantidad);
                if (element.data.m1) {
                    element.data.preciom1 = totalLinea;
                } else {
                    element.data.preciom1 = 0;
                }
                if (element.data.m2) {
                    element.data.preciom2 = totalLinea;
                } else {
                    element.data.preciom2 = 0;
                }
                if (element.data.m3) {
                    element.data.preciom3 = totalLinea;
                } else {
                    element.data.preciom3 = 0;
                }
                if (element.data.m4) {
                    element.data.preciom4 = totalLinea;
                } else {
                    element.data.preciom4 = 0;
                }
                if (element.data.m5) {
                    element.data.preciom5 = totalLinea;
                } else {
                    element.data.preciom5 = 0;
                }
            }

            this.total += Number(element.data.total);
            this.total1 += Number(element.data.preciom1);
            this.total2 += Number(element.data.preciom2);
            this.total3 += Number(element.data.preciom3);
            this.total4 += Number(element.data.preciom4);
            this.total5 += Number(element.data.preciom5);
        });
        this.total1 = this.total1 * (this.presupuesto.m1 / 100);
        this.total2 = this.total2 * (this.presupuesto.m2 / 100);
        this.total3 = this.total3 * (this.presupuesto.m3 / 100);
        this.total4 = this.total4 * (this.presupuesto.m4 / 100);
        this.total5 = this.total5 * (this.presupuesto.m5 / 100);
        this.total = this.total + this.total1 + this.total2 + this.total3 + this.total4 + this.total5;
        // this.total = this.total.toFixed(3);
        this.detalle = [...this.detalle];
        // console.log(this.detalle);
    }

    calcularTotales(currentNode: TreeNode) {
        if (currentNode.children.length > 0) {
            let precioNodoM1 = 0;
            let precioNodoM2 = 0;
            let precioNodoM3 = 0;
            let precioNodoM4 = 0;
            let precioNodoM5 = 0;
            let precioNodo = 0;
            let cantM1 = 0;
            let cantM2 = 0;
            let cantM3 = 0;
            let cantM4 = 0;
            let cantM5 = 0;
            currentNode.children.forEach(element => {
                this.calcularTotales(element);
                let totalLinea = 0;
                totalLinea = Number(element.data.precio) * Number(element.data.cantidad);
                precioNodo += totalLinea ;
                element.data.total = totalLinea;
                if (element.data.m1) {
                    precioNodoM1 += totalLinea;
                    cantM1++;
                }
                if (element.data.m2) {
                    precioNodoM2 += totalLinea;
                    cantM2++;
                }
                if (element.data.m3) {
                    precioNodoM3 += totalLinea;
                    cantM3++;
                }
                if (element.data.m4) {
                    precioNodoM4 += totalLinea;
                    cantM4++;
                }
                if (element.data.m5) {
                    precioNodoM5 += totalLinea;
                    cantM5++;
                }

            });
            if (cantM1 !== currentNode.children.length && cantM1 !== 0) {
                currentNode.data.m1state = true;
                currentNode.data.m1 = false;
            } else {
                currentNode.data.m1state = false;
                if (cantM1 === 0) {
                    currentNode.data.m1 = false;
                } else {
                    currentNode.data.m1 = true;
                }
            }
            if (cantM2 !== currentNode.children.length && cantM2 !== 0) {
                currentNode.data.m2state = true;
                currentNode.data.m2 = false;
            } else {
                currentNode.data.m2state = false;
                if (cantM2 === 0) {
                    currentNode.data.m2 = false;
                } else {
                    currentNode.data.m2 = true;
                }
            }
            if (cantM3 !== currentNode.children.length && cantM3 !== 0) {
                currentNode.data.m3state = true;
                currentNode.data.m3 = false;
            } else {
                currentNode.data.m3state = false;
                if (cantM3 === 0) {
                    currentNode.data.m3 = false;
                } else {
                    currentNode.data.m3 = true;
                }
            }
            if (cantM4 !== currentNode.children.length && cantM4 !== 0) {
                currentNode.data.m4state = true;
                currentNode.data.m4 = false;
            } else {
                currentNode.data.m4state = false;
                if (cantM4 === 0) {
                    currentNode.data.m4 = false;
                } else {
                    currentNode.data.m4 = true;
                }
            }
            if (cantM5 !== currentNode.children.length && cantM5 !== 0) {
                currentNode.data.m5state = true;
                currentNode.data.m5 = false;
            } else {
                currentNode.data.m5state = false;
                if (cantM5 === 0) {
                    currentNode.data.m5 = false;
                } else {
                    currentNode.data.m5 = true;
                }
            }
            precioNodoM1 *= currentNode.data.cantidad;
            precioNodoM2 *= currentNode.data.cantidad;
            precioNodoM3 *= currentNode.data.cantidad;
            precioNodoM4 *= currentNode.data.cantidad;
            precioNodoM5 *= currentNode.data.cantidad;
            currentNode.data.precio = precioNodo;
            currentNode.data.total = (currentNode.data.precio * currentNode.data.cantidad);
            currentNode.data.preciom1 = precioNodoM1;
            currentNode.data.preciom2 = precioNodoM2;
            currentNode.data.preciom3 = precioNodoM3;
            currentNode.data.preciom4 = precioNodoM4;
            currentNode.data.preciom5 = precioNodoM5;

        }
    }

    productoToTreeNode(producto: Producto): TreeNode {
        return {
            children: [],
            data: {
                activo: 1,
                aplicacion: this.aplicacionSeleccionado.descripcion,
                cantidad: this.cantidad,
                codigoparticular: producto,
                codigoAplicacion: this.aplicacionSeleccionado.codigo,
                descripcion: producto.descripcion,
                id: 0,
                idConjunto: 0,
                m1: false,
                m1Desc: '',
                m2: false,
                m2Desc: '',
                m3: false,
                m3Desc: '',
                m4: false,
                m4Desc: '',
                m5: false,
                m5Desc: '0',
                precio: producto.precios[0].precio,
                rubro: producto.rubro,
                tipoPrecio: producto.tipoPrecio,
                fechaPrecio: producto.precios[0].fecha
            }
        };
    }

    collapseAll() {
        this.detalle.forEach(element => {
            element.expanded = false;
            this.collapseChildren(element);
        });
        this.detalle = [...this.detalle];
    }

    collapseChildren(node: TreeNode) {
        if (node.children) {
            node.expanded = false;
            for (const cn of node.children) {
                this.collapseChildren(cn);
            }
        }
    }

    modifica(node: any) {
        // console.log(node.node.data);
        this.dModifica = true;
        this.nodeDescripcion = node.node.data.descripcion;
        this.nodeCantidad = node.node.data.cantidad;
        this.nodePrecio = node.node.data.precio;
    }

    confirmEdit() {
        this.modificaReg(this.detalle, this.selectednode1);
        this.dModifica = false;
    }

    modificaReg(currentNode: Array<TreeNode>, node: any) {
        if (currentNode.length > 0) {
            currentNode.forEach((element, index) => {
                this.modificaReg(element.children, node);
                if (node.data === element.data) {
                    if (node.parent == null) {
                        this.detalle[index].data.cantidad = this.nodeCantidad;
                        this.detalle[index].data.precio = this.nodePrecio;
                        this.calcularTotalesIni();
                    } else {
                        node.data.cantidad = this.nodeCantidad;
                        node.data.precio = this.nodePrecio;
                        this.calcularTotalesIni();
                    }
                }
            });
        }
        this.detalle = [...this.detalle];
    }

    showConfirm() {
        // this.sobreescribe();
        this.confirmationService.confirm({
            message: 'Ya existe!. Desea sobreescribirlo?',
            header: 'Confirmación de borrado',
            icon: 'pi pi-info-circle',
            accept: () => {
                console.log('Sobreescribe');
                this.sobreescribe();
            },
            reject: () => {
                console.log('No Sobreescribe');
            }
        });
    }

    antesGrabar() {
        this.presupuesto.items = this.detalle;
        this.presupuesto.cliente = this.codCliente;
        this.presupuesto.obra = this.codObra;
        this.presupuesto.version = this.version;
        this.presupuestoService.listarVersiones(this.codCliente, this.codObra).subscribe(
            data => {
                this.versiones = data;
                console.log(this.version);
                console.log(typeof(this.version));
                if (this.versiones.some(e => e.version === this.version)) {
                    console.log('Existe');
                    this.showConfirm();
                } else {
                    this.grabar();
                }
                /*if (this.obras.length>0){
                    this.showObra=false;
                }*/
            }
        );
    }

    sobreescribe() {
        this.presupuestoService.borrarPresupuesto(this.presupuesto).subscribe(
            res => {
                console.log(res);
                console.log('BOrrado');
                // this.msgs = [{severity: 'success', summary: 'Atención!', detail: 'Se borró.'}];
                this.grabar();
            },
            err => {
                console.log(JSON.stringify(err));
                this.messageService.add({severity: 'info', summary: 'Atención!', detail: 'No se pudo borrar la version a pisar.'});
                // this.msgs = [{severity: 'info', summary: 'Atención!', detail: 'No se pudo borrar la version a pisar.'}];
            }
        );
    }

    grabar() {
        console.log('Grabando');
        this.presupuestoService.guardar(this.presupuesto).subscribe(
            res => {
                console.log(JSON.stringify(res));
                this.messageService.add({severity: 'success', summary: 'Confirmado', detail: 'Se ha grabado el Presupuesto'});
                // this.msgs = [{severity: 'success', summary: 'Confirmado', detail: 'Se ha grabado el Presupuesto'}];
                this.versiones.push({'version': this.version});
            },
            err => {
                console.log(JSON.stringify(err));
                this.messageService.add({severity: 'info', summary: 'Atención!', detail: 'No se ha grabado'});
                // this.msgs = [{severity: 'info', summary: 'Atención!', detail: 'No se ha grabado'}];
            }
        );
    }

    comenzarLista() {
        this.detalle.forEach((element, index) => {
            if (element.children.length > 0) {
                this.armarLista(element);
            } else {
                // console.log(element.data);
                this.listaOrdenada.push(<Detalle>element.data);
            }
        });
        this.cols = [
            { field: 'tipoPrecio', header: 'Tipo Precio' },
            { field: 'descripcion', header: 'Descripcion' },
            { field: 'rubro', header: 'Rubro' },
            { field: 'aplicacion', header: 'Aplicacion' },
            { field: 'cantidad', header: 'Cantidad' },
            { field: 'precio', header: 'P.U.' },
            { field: 'fechaPrecio', header: 'Fecha PU' },
            { field: 'total', header: 'Total' }
        ];
        this.lista_ord = true;
    }

    armarLista(currentNode: TreeNode) {
        if (currentNode.children.length > 0) {
            currentNode.children.forEach(element => {
                this.armarLista(element);
            });
        } else {
            // console.log(currentNode.data);
            this.listaOrdenada.push(<Detalle>currentNode.data);
        }
    }
}
